<?

include 'conn.php';

$twochar = htmlspecialchars($_GET["state"]);

//$state = $_GET['state'];
//echo convert_state($state);

$twochar = strtolower($twochar);

$state = convert_state($twochar);

function convert_state($key) {
    $a2s = array( 
		'us'=>'United States',
		'al'=>'Alabama',
		'ak'=>'Alaska',
		'az'=>'Arizona',
		'ar'=>'Arkansas',
		'ca'=>'California',
		'co'=>'Colorado',
		'ct'=>'Connecticut',
		'de'=>'Delaware',
		'dc'=>'District of Columbia',
		'fl'=>'Florida',
		'ga'=>'Georgia',
		'hi'=>'Hawaii',
		'id'=>'Idaho',
		'il'=>'Illinois',
		'in'=>'Indiana',
		'ia'=>'Iowa',
		'ks'=>'Kansas',
		'ky'=>'Kentucky',
		'la'=>'Louisiana',
		'me'=>'Maine',
		'md'=>'Maryland',
		'ma'=>'Massachusetts',
		'mi'=>'Michigan',
		'mn'=>'Minnesota',
		'ms'=>'Mississippi',
		'mo'=>'Missouri',
		'mt'=>'Montana',
		'ne'=>'Nebraska',
		'nv'=>'Nevada',
		'nh'=>'New Hampshire',
		'nj'=>'New Jersey',
		'nm'=>'New Mexico',
		'ny'=>'New York',
		'nc'=>'North Carolina',
		'nd'=>'North Dakota',
		'oh'=>'Ohio',
		'ok'=>'Oklahoma',
		'or'=>'Oregon',
		'pa'=>'Pennsylvania',
		'ri'=>'Rhode Island',
		'sc'=>'South Carolina',
		'sd'=>'South Dakota',
		'tn'=>'Tennessee',
		'tx'=>'Texas',
		'ut'=>'Utah',
		'vt'=>'Vermont',
		'va'=>'Virginia',
		'wa'=>'Washington',
		'wv'=>'West Virginia',
		'wi'=>'Wisconsin',
		'wy'=>'Wyoming',
		'as'=>'American Samoa',
		'gu'=>'Guam',
		'mp'=>'Northern Mariana Islands',
		'pr'=>'Puerto Rico',
		'vi'=>'Virgin Islands',
		'um'=>'U.S. Minor Outlying Islands',
		'fm'=>'Federated States of Micronesia',
		'mh'=>'Marshall Islands',
		'pw'=>'Palau',
		'aa'=>'U.S. Armed Forces – Americas',
		'ae'=>'U.S. Armed Forces – Europe',
		'ap'=>'U.S. Armed Forces – Pacific',
		'cm'=>'Northern Mariana Islands',
		'cz'=>'Panama Canal Zone',
		'pi'=>'Philippine Islands',
		'tt'=>'Trust Territory of the Pacific Islands'
    );
    $array = (strlen($key) == 2 ? $a2s : array_flip($a2s));
    return $array[$key];
}



?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alarmregistration.com | <? echo $state; ?></title>

    <meta name="description" content="">
    <meta name="author" content="">

    <link href="../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../css/style.css" rel="stylesheet">
    
    <?php include("analytics.inc.php"); ?>


  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>
					<a href="index.php">Alarmregistration.com</a> | <? echo $state; ?> <small>Please Select Your City</small>
				</h1>
			</div>
			
            
            <div class="panel-group" id="panel-<? echo $twochar; ?>">
            <?
			
				$sql = "SELECT * FROM entries WHERE state_code = '".$twochar."' && approved = 'yes' ORDER BY cityname";
				
				$result = mysqli_query($conn, $sql);
				
				while($row = mysqli_fetch_assoc($result))
				{
					echo "<div class=\"panel panel-default\">";
					echo "<div class=\"panel-heading\"><a class=\"panel-title\" data-toggle=\"collapse\" data-parent=\"#panel-{$twochar}\" href=\"#panel-element-{$row['id']}\">{$row['cityname']}</a></div>";
					echo "<div id=\"panel-element-{$row['id']}\" class=\"panel-collapse collapse\">
						<div class=\"panel-body\">";
					echo "<p>" . $row['description'] . "</p>";
					
					echo "<p>";
					if($row['onlinereglink']){
						echo "<a href=\"{$row['onlinereglink']}\" class=\"btn btn-info\" role=\"button\" target=\"_blank\">Online Registration</a> &nbsp;";
					}
					
					/*if($row['onlinereglink'] && $row['printlink']){
						echo "&nbsp;&#124;&nbsp;";
					}*/
					
					if($row['printlink']){
						echo "<a href=\"{$row['printlink']}\" class=\"btn btn-info\" role=\"button\"  target=\"_blank\">Printable Registration Form</a> &nbsp;";
					}
					
					/*if($row['printlink'] && $row['jurisdictionlink']){
						echo "&nbsp;&#124;&nbsp;";
					}*/
					
					if($row['jurisdictionlink']){
						echo "<a href=\"{$row['jurisdictionlink']}\" class=\"btn btn-info\" role=\"button\" target=\"_blank\">Jurisdiction Website</a> &nbsp;";
					}
					
					/*if( $row['ordinancelink'] ){
						echo "&nbsp;&#124;&nbsp;";
					}*/
					
					if($row['ordinancelink']){
						echo "<a href=\"{$row['ordinancelink']}\" class=\"btn btn-info\" role=\"button\" target=\"_blank\">Ordinance</a>";
					}
					
					"</p>";
					echo "</div></div></div>";
				}
			
			?>
            </div>

		</div>
	</div>
</div>

  <div style="text-align:center; padding-bottom:15px;">
    <h1><a href="add.php?state=<?=$twochar; ?>">Don't see your jurisdiction's ordinance? Click here to upload information for it.</a></h1>
  </div>

<?php include("footer.inc.php"); ?>

    <script src="../../../../js/jquery.min.js"></script>
    <script src="../../../../js/bootstrap.min.js"></script>
    <script src="../../../../js/scripts.js"></script>
  </body>
</html>

<? include 'close_conn.php'; ?>