<!doctype html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <title>Alarmregistration.com</title>
  <meta name="description" content="">
  <meta name="author" content="">
    
  <link href='https://fonts.googleapis.com/css?family=Droid+Sans' rel='stylesheet' type='text/css'>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css" rel="stylesheet" type="text/css" />

  <style type="text/css">
    html {
      box-sizing: border-box;
    }
    *, *:before, *:after {
      box-sizing: inherit;
    }

    body {
      font-family: 'Droid Sans', sans-serif;
      background:#f2f2f2;
      font-size:14px;
      line-height:21px;
    }

    .container {
      width: 960px;
      margin:20px auto;
    }

    @media only screen and (min-width: 768px) and (max-width: 1000px) {
      .container {
        width: 768px;
      }
    }
    @media only screen and (max-width: 767px) {
      .container {
        width: 420px;
      }
    }
    @media only screen and (max-width: 480px) {
      .container {
        width: 300px;
      }
    }

    a img {
      border:none;
    }

    h1, h2, h3, h4, h5, h6{ 
      font-weight:normal; 
    }
    h1{ 
      font-size:26px; 
      line-height:32px; 
    }
    p, ul{
      margin-bottom:10px;
    }

  </style>
      
      <link href="css/bootstrap.min.css" rel="stylesheet" rel="stylesheet" type="text/css" />
      
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>

  <!-- Jquery is required, embed on your page if not already - don't embed 2 versions -->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.0.0-beta1/jquery.min.js" type="text/javascript"></script>
  <!-- End Jquery -->

  <!-- Map scripts - add the below to your page -->
  <link href="lg-map/map.css" rel="stylesheet" type="text/css" />
  <script src="lg-map/raphael.js" type="text/javascript"></script>
  <script src="lg-map/scale.raphael.js" type="text/javascript"></script>
  <script src="lg-map/lg-map.js" type="text/javascript"></script>
  <!-- End Map scripts -->

    <?php include("analytics.inc.php"); ?>

</head>

<body>

  <div class="container">
  
  <div style="width:500px; margin-left:auto; margin-right:auto;">
        <div style="text-align:center; float:left; padding-right:20px;"><h1>Alarmregistration.org</h1>&nbsp;&nbsp;&nbsp;</div>
        
        <div style="float:left;">
                <div class="dropdown">
                <button class="btn btn-lg btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Please Select Your State
                </button>
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                            <a class="dropdown-item" href="dir.php?state=AK">Alaska</a>
                            <a class="dropdown-item" href="dir.php?state=AL">Alabama</a>
                            <a class="dropdown-item" href="dir.php?state=AR">Arkansas</a>
                            <a class="dropdown-item" href="dir.php?state=AZ">Arizona</a>
                            <a class="dropdown-item" href="dir.php?state=CA">California</a>
                            <a class="dropdown-item" href="dir.php?state=CO">Colorado</a>
                            <a class="dropdown-item" href="dir.php?state=CT">Connecticut</a>
                            <a class="dropdown-item" href="dir.php?state=DC">District of Columbia</a>
                            <a class="dropdown-item" href="dir.php?state=DE">Delaware</a>
                            <a class="dropdown-item" href="dir.php?state=FL">Florida</a>
                            <a class="dropdown-item" href="dir.php?state=GA">Georgia</a>
                            <a class="dropdown-item" href="dir.php?state=HI">Hawaii</a>
                            <a class="dropdown-item" href="dir.php?state=IA">Iowa</a>
                            <a class="dropdown-item" href="dir.php?state=ID">Idaho</a>
                            <a class="dropdown-item" href="dir.php?state=IL">Illinois</a>
                            <a class="dropdown-item" href="dir.php?state=IN">Indiana</a>
                            <a class="dropdown-item" href="dir.php?state=KS">Kansas</a>
                            <a class="dropdown-item" href="dir.php?state=KY">Kentucky</a>
                            <a class="dropdown-item" href="dir.php?state=LA">Louisiana</a>
                            <a class="dropdown-item" href="dir.php?state=MA">Massachusetts</a>
                            <a class="dropdown-item" href="dir.php?state=MD">Maryland</a>
                            <a class="dropdown-item" href="dir.php?state=ME">Maine</a>
                            <a class="dropdown-item" href="dir.php?state=MI">Michigan</a>
                            <a class="dropdown-item" href="dir.php?state=MN">Minnesota</a>
                            <a class="dropdown-item" href="dir.php?state=MO">Missouri</a>
                            <a class="dropdown-item" href="dir.php?state=MS">Mississippi</a>
                            <a class="dropdown-item" href="dir.php?state=MT">Montana</a>
                            <a class="dropdown-item" href="dir.php?state=NC">North Carolina</a>
                            <a class="dropdown-item" href="dir.php?state=ND">North Dakota</a>
                            <a class="dropdown-item" href="dir.php?state=NE">Nebraska</a>
                            <a class="dropdown-item" href="dir.php?state=NH">New Hampshire</a>
                            <a class="dropdown-item" href="dir.php?state=NJ">New Jersey</a>
                            <a class="dropdown-item" href="dir.php?state=NM">New Mexico</a>
                            <a class="dropdown-item" href="dir.php?state=NV">Nevada</a>
                            <a class="dropdown-item" href="dir.php?state=NY">New York</a>
                            <a class="dropdown-item" href="dir.php?state=OH">Ohio</a>
                            <a class="dropdown-item" href="dir.php?state=OK">Oklahoma</a>
                            <a class="dropdown-item" href="dir.php?state=OR">Oregon</a>
                            <a class="dropdown-item" href="dir.php?state=PA">Pennsylvania</a>
                            <a class="dropdown-item" href="dir.php?state=PR">Puerto Rico</a>
                            <a class="dropdown-item" href="dir.php?state=RI">Rhode Island</a>
                            <a class="dropdown-item" href="dir.php?state=SC">South Carolina</a>
                            <a class="dropdown-item" href="dir.php?state=SD">South Dakota</a>
                            <a class="dropdown-item" href="dir.php?state=TN">Tennessee</a>
                            <a class="dropdown-item" href="dir.php?state=TX">Texas</a>
                            <a class="dropdown-item" href="dir.php?state=UT">Utah</a>
                            <a class="dropdown-item" href="dir.php?state=VA">Virginia</a>
                            <a class="dropdown-item" href="dir.php?state=VT">Vermont</a>
                            <a class="dropdown-item" href="dir.php?state=WA">Washington</a>
                            <a class="dropdown-item" href="dir.php?state=WI">Wisconsin</a>
                            <a class="dropdown-item" href="dir.php?state=WV">West Virginia</a>
                            <a class="dropdown-item" href="dir.php?state=WY">Wyoming</a>
                        </div>
                </div>
        </div>
	</div>
<div style="clear:both; width:100%;"></div>

    <!-- Map html - add the below to your page -->
    <div class="lg-map-wrapper" data-map="lg-map/usa.js">
      <div id="lg-map"></div>
      <div class="lg-map-text"></div>
    </div>
    <!-- End Map html -->

  </div>
  
  <div style="text-align:center; padding-bottom:15px;">  
    <a href="add.php" class="btn btn-info" role="button">Add an Ordinance</a> &nbsp; <a href="news.php" class="btn btn-info" role="button">News</a> &nbsp; <a href="alarmcompanies.php" class="btn btn-info" role="button">Alarm Companies</a> &nbsp; <a href="misc.php" class="btn btn-info" role="button">Misc</a>
  </div>
  
<?php include("footer.inc.php"); ?>

</body>

</html>