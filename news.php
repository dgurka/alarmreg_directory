<?

include 'conn.php';

?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Alarmregistration.com | News</title>

    <meta name="description" content="">
    <meta name="author" content="">

    <link href="../../../../css/bootstrap.min.css" rel="stylesheet">
    <link href="../../../../css/style.css" rel="stylesheet">
    
    <?php include("analytics.inc.php"); ?>


  </head>
  <body>

    <div class="container-fluid">
	<div class="row">
		<div class="col-md-12">
			<div class="page-header">
				<h1>
					<a href="index.php">Alarmregistration.com</a> | News
				</h1>
			</div>
			
            
            <div class="panel-group" id="panel-news">
            <?
			
				$sql = "SELECT * FROM news_entries ORDER BY date";
				
				$result = mysqli_query($conn, $sql);
				
				while($row = mysqli_fetch_assoc($result))
				{
					echo "<div class=\"panel panel-default\">";
					echo "<div class=\"panel-heading\"><a class=\"panel-title\" data-toggle=\"collapse\" data-parent=\"#panel-news\" href=\"#panel-element-{$row['id']}\">{$row['title']}</a></div>";
					echo "<div id=\"panel-element-{$row['id']}\" class=\"panel-collapse collapse\">
						<div class=\"panel-body\">";
					echo "<p>" . $row['description'] . "</p>";
					
					echo "<p>";
					if($row['link']){
						echo "<a href=\"{$row['link']}\" class=\"btn btn-info\" role=\"button\" target=\"_blank\">Read More...</a> &nbsp;";
					}
					"</p>";
					
					echo "</div></div></div>";
				}
			
			?>
            </div>

		</div>
	</div>
</div>

  <div style="text-align:center; padding-bottom:15px;">

  </div>

<?php include("footer.inc.php"); ?>

    <script src="../../../../js/jquery.min.js"></script>
    <script src="../../../../js/bootstrap.min.js"></script>
    <script src="../../../../js/scripts.js"></script>
  </body>
</html>

<? include 'close_conn.php'; ?>