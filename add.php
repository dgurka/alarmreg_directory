<?

include 'conn.php';

$twochar = htmlspecialchars($_GET["state"]);

if(isset($twochar)){
	$s = htmlspecialchars($_GET["state"]);
} else {	
	$s = "";
}

function state_dropdown($sc){
	echo '<select name="state_code">';
				
				$a2s = array( 
					'al'=>'Alabama',
					'ak'=>'Alaska',
					'az'=>'Arizona',
					'ar'=>'Arkansas',
					'ca'=>'California',
					'co'=>'Colorado',
					'ct'=>'Connecticut',
					'de'=>'Delaware',
					'dc'=>'District of Columbia',
					'fl'=>'Florida',
					'ga'=>'Georgia',
					'hi'=>'Hawaii',
					'id'=>'Idaho',
					'il'=>'Illinois',
					'in'=>'Indiana',
					'ia'=>'Iowa',
					'ks'=>'Kansas',
					'ky'=>'Kentucky',
					'la'=>'Louisiana',
					'me'=>'Maine',
					'md'=>'Maryland',
					'ma'=>'Massachusetts',
					'mi'=>'Michigan',
					'mn'=>'Minnesota',
					'ms'=>'Mississippi',
					'mo'=>'Missouri',
					'mt'=>'Montana',
					'ne'=>'Nebraska',
					'nv'=>'Nevada',
					'nh'=>'New Hampshire',
					'nj'=>'New Jersey',
					'nm'=>'New Mexico',
					'ny'=>'New York',
					'nc'=>'North Carolina',
					'nd'=>'North Dakota',
					'oh'=>'Ohio',
					'ok'=>'Oklahoma',
					'or'=>'Oregon',
					'pa'=>'Pennsylvania',
					'ri'=>'Rhode Island',
					'sc'=>'South Carolina',
					'sd'=>'South Dakota',
					'tn'=>'Tennessee',
					'tx'=>'Texas',
					'ut'=>'Utah',
					'vt'=>'Vermont',
					'va'=>'Virginia',
					'wa'=>'Washington',
					'wv'=>'West Virginia',
					'wi'=>'Wisconsin',
					'wy'=>'Wyoming'/*,
					'as'=>'American Samoa',
					'gu'=>'Guam',
					'mp'=>'Northern Mariana Islands',
					'pr'=>'Puerto Rico',
					'vi'=>'Virgin Islands',
					'um'=>'U.S. Minor Outlying Islands',
					'fm'=>'Federated States of Micronesia',
					'mh'=>'Marshall Islands',
					'pw'=>'Palau',
					'aa'=>'U.S. Armed Forces – Americas',
					'ae'=>'U.S. Armed Forces – Europe',
					'ap'=>'U.S. Armed Forces – Pacific',
					'cm'=>'Northern Mariana Islands',
					'cz'=>'Panama Canal Zone',
					'pi'=>'Philippine Islands',
					'tt'=>'Trust Territory of the Pacific Islands'*/
				);
				
                foreach($a2s as $key => $value):
                	echo '<option value="'.$key.'"';
					
					if($sc == $key){
						echo ' selected ';
					}
				
					echo '>'.$value.'</option>'; //close your tags!!
                endforeach;
               
           echo '</select>';
}

function doEmail($city, $state){
	
	$to_email = "frank@enablepoint.com,dgurka@enablepoint.com";
	$from_email = "noreply@alarmregistration.com";
	$subject =  "AlarmRegistration.com Jurisdiction Submission";
	
	$mailheader = "From: noreply@alarmregistration.com\r\n"; 
	$mailheader .= "Content-type: text/html; charset=iso-8859-1\r\n"; 
	
	
	//$message = clean($_POST['message']);
	$message = "New jurisdiction has been added for: ".$city.", ".$state." and is awaiting approval.";
	$message = nl2br($message);
	
	//$body = "Name: $name\n E-Mail: $from_email\n Message:\n $message\n\n";			
	
	$sendmail = mail("frank@enablepoint.com", $subject, $message, "From: <$from_email>");
	$sendmail2 = mail("dgurka@enablepoint.com", $subject, $message, "From: <$from_email>");

}

?>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title>Alarmregistration.com | Submit a Jurisdiction</title>
<meta name="description" content="">
<meta name="author" content="">
<link href="css/bootstrap.min.css" rel="stylesheet">
<link href="css/style.css" rel="stylesheet">
<?php include("analytics.inc.php"); ?>

<script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>
<div class="container-fluid">
  <div class="row">
    <div class="col-md-12">
      <div class="page-header">
        <h1> <a href="index.php">Alarmregistration.com</a> | <small>Submit a Jurisdiction</small> </h1>
      </div>
      <div style="padding-left:15px;">
      
		<?php
			if($_SERVER["REQUEST_METHOD"] == "POST"){
				
			/*require_once('recaptchalib.php');
		  $privatekey = "6Lc3qsgSAAAAALU6LiLZrIMHOsogmtyEb-6TOhgh";
		  $resp = recaptcha_check_answer ($privatekey,
										$_SERVER["REMOTE_ADDR"],
										$_POST["recaptcha_challenge_field"],
										$_POST["recaptcha_response_field"]);
			
		
			  if (!$resp->is_valid) {*/
			  
			if(isset($_POST['g-recaptcha-response'])){
				$captcha=$_POST['g-recaptcha-response'];
			}
			if(!$captcha){
				echo "<p style='color:red; font-weight:bold;'>The reCAPTCHA failed. Please try again.</p>";
				exit();
			}
			
			$response=file_get_contents("https://www.google.com/recaptcha/api/siteverify?secret=6Lcq1UcUAAAAAIKYw1VqEQ3BJLYMJyyONzPR-OLJ&response=".$captcha."&remoteip=".$_SERVER['REMOTE_ADDR']);
			
			if($response.success==false)            
			{
				// What happens when the CAPTCHA was entered incorrectly
				//echo "<p style='color:red; font-weight:bold;'>The reCAPTCHA wasn't entered correctly. Please try again.</p>";
				echo "<p style='color:red; font-weight:bold;'>The reCAPTCHA failed. Please try again.</p>";
				//echo "<p>result: ".$captcha_success."</p>";
			  } else {									
					$state_code = htmlspecialchars($_POST["state_code"]);
					$cityname = htmlspecialchars($_POST["cityname"]);
					$description = htmlspecialchars($_POST["description"]);
					$printlink = htmlspecialchars($_POST["printlink"]);
					$onlinereglink = htmlspecialchars($_POST["onlinereglink"]);
					$jurisdictionlink = htmlspecialchars($_POST["jurisdictionlink"]);
					$ordinancelink = htmlspecialchars($_POST["ordinancelink"]);
					$email = htmlspecialchars($_POST["email"]);
					
					$sql = "INSERT INTO entries (id, state_code, cityname, description, printlink, onlinereglink, jurisdictionlink, ordinancelink, approved, addedby) VALUES (NULL, '".$state_code."', '".$cityname."', '".$description."', '".$printlink."', '".$onlinereglink."', '".$jurisdictionlink."', '".$ordinancelink."', 'no', '".$email."');";
										
					//mysqli_query($conn,$sql);
					
					if ($conn->query($sql) === TRUE) {
						echo "<p style='color:green; font-weight:bold;'>Success. Your submission has been received, and will be displayed pending review.</p>";
						doEmail($cityname,$state_code);
					} else {
						echo "Error: " . $sql . "<br>" . $conn->error;
					}
				
					
				}
			}
        ?>
      
      <form action="<?=$PHP_SELF;?>" method="post" name="add" id="add">
        <p>
          <label for="state_code">State:</label>
        </p>
        <p>
			<? state_dropdown($twochar); ?>
        </p>
        <p>
          <label for="cityname">City Name:</label>
        </p>
        <p>
          <input type="text" name="cityname" id="cityname">
        </p>
        <p>
          <label for="description">Description:</label>
        </p>
        <p>
    		<textarea name="description" cols="50" rows="8" id="description"></textarea>
        </p>
        <p>
          <label for="onlinereglink">Online Registration Link:</label>
        </p>
        <p>
          <input type="text" name="onlinereglink" id="onlinereglink">
        </p>
        <p>
          <label for="printlink">Printable Registration Form Link:</label>
        </p>
        <p>
          <input type="text" name="printlink" id="printlink">
        </p>
        <p>
          <label for="jurisdictionlink">Jurisdiction Website Link:</label>
        </p>
        <p>
          <input type="text" name="jurisdictionlink" id="jurisdictionlink">
        </p>
        <p>
          <label for="ordinancelink">Ordinance Link:</label>
        </p>
        <p>
          <input type="text" name="ordinancelink" id="ordinancelink">
        </p>
        <p>
          <label for="email">Contact Email (NEVER displayed publicly):</label>
        </p>
        <p>
          <input type="text" name="email" id="email">
        </p>
		<p>Please enter the text you see below:</p>
		<p><?PHP /*require_once('recaptchalib.php');
                $publickey = "6Lc3qsgSAAAAAPSKnaR1KvtyGmQQcolTRQB4ryAt"; // you got this from the signup page
                echo recaptcha_get_html($publickey);*/ ?>                
                <div class="g-recaptcha" data-sitekey="6Lcq1UcUAAAAAO6lukGEFKvYFsZb4mArqudC6zQK"></div>
		</p>
        <p>
          <input type="submit" name="submit" id="submit" value="Submit">
          <input type="reset" name="reset" id="reset" value="Reset">
        </p>
        <p><a href="index.php">Return to alarmregistration.org</a></p>
      </form></div>
    </div>
  </div>
</div>
<?php include("footer.inc.php"); ?>
<script src="js/jquery.min.js"></script> 
<script src="js/bootstrap.min.js"></script> 
<script src="js/scripts.js"></script>
</body>
</html>
<? include 'close_conn.php'; ?>