-- phpMyAdmin SQL Dump
-- version 4.0.10.18
-- https://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jun 07, 2017 at 02:35 PM
-- Server version: 5.6.35-cll-lve
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `alarmreg`
--

-- --------------------------------------------------------

--
-- Table structure for table `entries`
--

CREATE TABLE IF NOT EXISTS `entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `state_code` varchar(2) NOT NULL,
  `cityname` text NOT NULL,
  `description` text NOT NULL,
  `printlink` text NOT NULL,
  `onlinereglink` text NOT NULL,
  `jurisdictionlink` text NOT NULL,
  `ordinancelink` text NOT NULL,
  `approved` varchar(11) NOT NULL DEFAULT 'yes',
  `addedby` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=30 ;

--
-- Dumping data for table `entries`
--

INSERT INTO `entries` (`id`, `state_code`, `cityname`, `description`, `printlink`, `onlinereglink`, `jurisdictionlink`, `ordinancelink`, `approved`, `addedby`) VALUES
(1, 'mi', 'Livonia', 'Livonia does not require alarm registration.  There is a false alarm fine schedule for this city. ', 'http://livoniapd.com/bureaus/administration/', '', '', '', 'yes', ''),
(2, 'mi', 'Lansing', 'Lansing Requires alarm registration.  When you register you are considered registered for the life of the alarm system, unless you change alarm companies or move.   The online registration link is below.  If you register online you do not have to pay immediately, you will be invoiced.   if you change alarm companies or move, you have to re register and pay again. <br><br>The registration fee is $25.  <br><br> Lansing has false alarm fines that are assessed.  The fee schedule is counted by the number of alarms in the last 365 days.  The first two are free, then the fines start at 3:\r\n<br><br>\r\n3rd Alarm = $35<br><br>\r\n4th Alarm = $65<br><br>\r\n5th Alarm = $100<br><br>\r\n6th Alarm = $150 <br><br>\r\n7th Alarm = $200<br><br>\r\n8th and higher = $200 <br><br>\r\n\r\nIf you have a burglar AND fire alarm combination, on the SAME control panel, you can send in your certificate of installation instead of payment.  <br>\r\nFor alarm registration or false alarm invoice inquiries, call the alarm support department at (734) 619-0550, or email support [at] alarmsupport.org<br><br>\r\nIf you have an invoice number, you can pay for your false alarm OR your registration (with a registration invoice) at the below link:\r\n<br><br>\r\nhttps://client.pointandpay.net/web/LansingMI\r\n<br>\r\n\r\n\r\n\r\n', 'https://drive.google.com/open?id=0BzM0qrGsLahQb2oyYzdYcWhIRzA', 'http://lansing.alarmreg.com', 'http://www.lansingmi.gov', 'https://www.municode.com/library/mi/lansing/codes/code_of_ordinances?nodeId=COOR_PT8BURETACO_TIT2BURE_CH804ALSY', 'yes', ''),
(3, 'fl', 'Naples', 'Naples Florida CITY (the County requires registration if you live outside of city limits)  does not require registrations.  False Alarm Fines are as follows:\r\n<br><br>\r\nSec. 20-34. - Fees for false alarms.\r\n(a)\r\nUpon a response to any false burglar or robbery alarm by the police department, the police department shall charge and collect from the person having or maintaining such alarm on a premises owned or occupied by such person a fee as follows:\r\n<br>\r\n(1)\r\nFor a response to false alarm at premises where no false alarm has occurred within the preceding six-month period, referred to as a "1st response," no fee shall be charged. However, the person having or maintaining such alarm shall, within three working days after notice, make a written report to the police department on a form to be provided by the police department, setting forth the cause of such false alarm, the corrective action taken, whether such alarm has been inspected by an authorized serviceperson and such other information as the police department may reasonably require to determine the cause of such false alarm and the corrective action necessary.<br>\r\n(2)\r\nFor a 2nd response to a false alarm within six months after a 1st response, a fee as set forth in appendix A to this Code shall be charged and a written report shall be required as for a 1st response.<br>\r\n(3)\r\nFor a 3rd response to a false alarm within six months after the 1st response and for each succeeding response within six months after the last response, a fee as set forth in appendix A to this Code shall be charged. If such 3rd false alarm or any such succeeding false alarm results from the failure of the owner or manager to take necessary corrective action, the police department may order the disconnection of such alarm system, and it shall be unlawful to reconnect such alarm system until such corrective action is taken, provided that no disconnection shall be ordered for any premises required by law to have an alarm system in operation.<br>\r\n(b)\r\nFor the purpose of collecting the fees for false alarms, a notice shall be left with the owner, operator or agent of the premises or, in such person''s absence, left at a conspicuous location on the premises, notifying the owner or agent that the police responded to the location for a false alarm and that a follow-up false alarm notice will be sent via mail, setting forth the requirements of this article, and the amount of the fees, and indicating whether this is the 1st, 2nd, 3rd or later response within a six-month period. The follow-up notice shall state that the fee may be paid within 15 days, as provided in section 20-35, and that, if not paid, the fees will become a lien on the property protected by the alarm system, unless within 15 days from the date of such false alarm an authorized person appears at police headquarters, during the hours between 8:00 a.m. and 5:00 p.m., Monday through Friday, or at some other prearranged time, before an officer designated by the chief of police to receive such objections, to present an appropriate defense to the assessment of the fee. Such follow-up notice shall also state that any written or tangible evidence tending to support a defense should be presented at the same time. If the operator of the premises is leasing the premises from the owner thereof, the police department shall mail a copy of the notice to the owner of the property at the owner''s address as it appears on the current tax roll. An appropriate defense shall be considered to be any cause that was completely and utterly beyond the control of the owner or operator to prevent, such as an act of God. Failure of the system to be in proper operating condition shall not be a proper defense. If no appearance is made and the fees are not paid, the lien shall be recorded as provided in section 20-35.\r\n<br>\r\n(Code 1957, § 15-33; Code 1994, § 34-39; Ord. No. 10-12812, § 10, 11-17-2010)', 'https://www.municode.com/library/fl/naples/codes/code_of_ordinances?nodeId=PTIICOOR_CH20EMSE_ARTIIBUROALSY_S20-34FEFAAL', 'Registration not required', '', '', 'yes', ''),
(4, 'ca', 'Tritech CAD software', 'CrimeMapping and/or CAD software company', 'http://www.tritech.com/products/inform/inform-cad-911', 'Not Available', '', '', 'yes', ''),
(5, 'mn', 'Crow Wing County', '<b>Crow Wing County requires registration.  There is a $20 Annual Fee.  You will have to renew each year. The fee is for both new and renewals. </b> <BR><BR>\r\n\r\nThe Crow Wing County Sheriff''s Office online alarm registration system is now Live.  you can click the links below this text to register online or register by paper.  \r\n<br><br>\r\n<b>For online registration support, call 734-619-0550 </b><br><br>\r\n\r\nHere are some news releases related to the online system:\r\n<br><br>\r\n <a href="http://www.brainerddispatch.com/news/4087558-crow-wing-county-introduces-online-alarm-registration">Brainerd Dispatch </a>\r\n<br><br>\r\n<a href="http://www.messagemedia.co/millelacs/news/local/new-online-alarm-registration/article_33fa1180-58c3-11e6-a5e7-a72df80411b2.html"> MilleLacs Messenger </a>\r\n\r\n<br><br>\r\n\r\n', 'http://crowwing.us/DocumentCenter/View/853', 'http://alarms.crowwing.us\r\n', 'http://www.crowwing.us', '', 'yes', ''),
(6, 'mn', 'Minneapolis', 'Minneapolis has a false alarm fee structure, see the print Link below. No registration requirements are found.  An inquiry was made on 7/7/2016 to busineslicenses@minneapolismn.gov', 'http://www.minneapolismn.gov/licensing/business-licensing_falsealarms', 'busineslicenses@minneapolismn.gov', '', '', 'yes', ''),
(7, 'fl', 'Coral Gables', 'Coral Gables requires registration.  The registration form can be found below on the print link.   The fee schedule is hard to find, we found it in a pdf that is located: <br><br>\r\nhttp://coralgables.com/modules/showdocument.aspx?documentid=719  ', 'http://www.coralgables.com/modules/showdocument.aspx?documentid=12835', '', '', '', 'yes', ''),
(8, 'Fl', 'St Johns County including St Augustine', 'ST Johns includes St Augustine, Pondre Vedra, and all communities in the county. ', '', 'http://stjohns.alarmreg.com', 'http://www.clk.co.st-johns.fl.us/minrec/ordinancebooks/2010/ORD2010-15.pdf', '', 'yes', ''),
(9, 'MI', 'Ann Arbor', 'see ordinance on a2gov.org', 'http://www.a2gov.org/departments/police/Documents/AlarmConnectionPermit_2017_fillable.pdf', 'http://a2alarms.com\r\n', 'http://a2gov.org', '', 'yes', ''),
(10, 'Mi', 'City of Taylor, Michigan', 'The City of Taylor has been enforcing an existing false alarm ordinance since the beginning of the calendar year 2016.  The first two false alarms are not charged, and a warning letter is sent out to the alarm address on the first false alarm. <br><br>There is no requirement to register an alarm system in Taylor as of this writing last updated 2/23/2017.<br><br>\r\n\r\nthe false alarm fine schedule is as follows:<br><br>\r\n1st No charge, letter sent<br>\r\n2cd No charge, letter sent<br>\r\n3rd $100 fee<br>\r\n4th $200 fee<br>\r\n5th $300 fee<br>\r\n6th $400 fee<br>\r\n7th $400 fee<br>\r\n8th $500 fee<br>\r\n9th $500 fee<br>\r\n10th $600 fee<br>\r\n11th and above = $700 fee<br><br>\r\n\r\nThere is an appeals process for false alarm disputes.  It is taken from the ordinance below:<br><br>\r\n\r\nArticle III Chapter 18\r\nAny alarm user against whom a service fee has been assessed, as provided for in subsection (e) of this Section, who believes that said service fee has been improperly assessed, may appeal the assessment to an appeals board made up of three (3) members consisting of the fire chief and the police chief, or their designees, and a representative selected by the Mayor. Any such appeal shall be in writing and shall be filed with the City clerk within thirty (30) days of the date of the invoice for said service fee. If the appeal board determines that that the alarm was erroneously determined to be a false alarm, as that term is defined in this Section, then the service fee shall be waived. If the appeal board determines that alarm was a false alarm, then the service fee shall not be waived. The findings and the decision of the appeal board shall be in writing and filed with the office of the chief of police. The alarm user shall be provided with a copy of the decision by the appeal board. The appeal board’s decision is subject to review in the local District Court upon the filing of a proceeding in that Court within thirty (30) days of the decision of the appeal board. <br><br>\r\n\r\nSupport for this jurisdiction can be received by calling 734-619-0550 or email support (at) alarmsupport.org  <br><br>\r\n\r\nThe Municode.com ordinance appears to be out of date and has not been updated since the ordinance changed in 2016. <br><br>\r\n\r\nThere is a link on this entry that links to an updated ordinance on the city of taylor site. ', '', '', 'http://www.cityoftaylor.com/1056/Portal', 'http://www.taylor.lib.mi.us/1033/Excessive-False-Alarms', 'yes', ''),
(20, 'Mi', 'Farmington Hills, Mi', 'The City of Farmington Hills bills for false alarms.  The ordinance is attached to this listing. ', '', '', '', 'https://drive.google.com/file/d/0BzM0qrGsLahQeEdZeU00LWl3NWs/view?usp=sharing\r\n', 'yes', ''),
(18, 'Il', 'Oak Park', 'Oak Park requires an alarm permit.  ', '', 'http://www.alarmregistration.com', '', '', 'no', 'frank@wsbtm.com'),
(19, 'fl', 'Collier County Sheriff''s Office', 'Collier County requires the ALARM COMPANY to register alarms for the county.  The alarm owner does not have to register.  There are false alarm fines that are assessed to the alarm owner.  ', '', '', '', '', 'yes', ''),
(17, 'ca', 'Oxnard', 'Oxnard California is planning to change their ordinance.  RFP is attached. ', 'pintablereg.com', 'live-oars-demo.alarmreg.com', 'http://www.falsealarmsoftware.com', 'falsealarmsoftware.com', 'no', 'frank@wsbtm.com'),
(21, 'mi', 'Waterford Township', 'Waterford requires alarm permits. They are a one time charge and do not expire.   False Alarm Fees are also assessed as follows:\r\n<br>\r\n<br>\r\nFirst and second alarm = no fine\r\n<br>\r\nThird alarm = $25 <br>\r\nfourth alarm = $50 <br>\r\nFifth alarm = $50 <br>\r\nSixth and higher = $75 <br>\r\n<br>\r\n\r\n\r\n', '', 'http://waterford.alarmreg.com', 'https://waterfordmi.gov/274/Code-of-Ordinances', 'https://drive.google.com/file/d/0BzM0qrGsLahQM20wRllEekJwdmM/view?usp=sharing', 'yes', 'frankafarren@gmail.com'),
(29, 'Ca', 'Santa Monica', '', '', '', 'https://www.santamonicapd.org/Content.aspx?id=52831\r\n', '', 'yes', ''),
(26, 'Ca', 'Palm Springs Ca', '', '', 'https://palmspringsalarm.citysupport.org/licenses', '', '', 'yes', 'Frank'),
(27, 'Ca', 'Oxnard', '', 'https://www.oxnardpd.org/wp-content/uploads/2016/10/alarm-permit1.pdf\r\n', '', '', 'https://www.oxnardpd.org/wp-content/uploads/2013/02/alarm-ordinance-2002.pdf', 'yes', ''),
(28, '', 'LA Times News Article', '', 'http://articles.latimes.com/2000/aug/09/local/me-1189', '', '', '', 'yes', '');

-- --------------------------------------------------------

--
-- Table structure for table `misc_entries`
--

CREATE TABLE IF NOT EXISTS `misc_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `link` text NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `misc_entries`
--

INSERT INTO `misc_entries` (`id`, `title`, `description`, `link`, `date`) VALUES
(1, '', '', '', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `news_entries`
--

CREATE TABLE IF NOT EXISTS `news_entries` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `description` text NOT NULL,
  `link` text NOT NULL,
  `date` date DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `news_entries`
--

INSERT INTO `news_entries` (`id`, `title`, `description`, `link`, `date`) VALUES
(1, '', '', '', NULL);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
